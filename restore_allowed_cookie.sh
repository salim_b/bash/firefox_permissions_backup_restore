#!/bin/bash
#
# Restore Firefox' allowed origins for cookie and site data
#
# Parameters:
#
# - `$1`: Path (dir) to the Firefox profile directory.
# - `$2`: Path to the *allowed Firefox cookie and site data origins* backup file to be restored.

# ensure 1st param is given
if [[ -z "$1" ]] ; then
  echo "No primary argument provided. You need to specify the path to a Firefox profile directory."
  exit 1
fi
# ensure 2nd param is given
if [[ -z "$2" ]] ; then
  echo "No secondary argument provided. You need to specify the filepath to a backup of allowed Firefox cookie origins."
  exit 1
fi

## ensure all required CLI tools are available
if ! command -v grep >/dev/null ; then
  echo "\`grep\` is required but not found on PATH."
  exit 1
fi
if ! command -v notify-send >/dev/null ; then
  echo "\`notify-send\` is required but not found on PATH."
  exit 1
fi
if ! command -v sqlite3 >/dev/null ; then
  echo "\`sqlite3\` is required but not found on PATH."
  exit 1
fi

db="$1/permissions.sqlite"

if ! sqlite3 -batch "$db" ".dbinfo" 2>&1 | grep --quiet 'database is locked' ; then

  next_id=$(sqlite3 -batch "$db" "SELECT MAX(id) FROM moz_perms;")
  exisiting_origins=$(sqlite3 -batch "$db" "SELECT origin FROM moz_perms WHERE type = 'cookie' AND permission = '1';")

  while IFS= read -r line
  do
    origin=$(echo "$line" | grep --perl-regexp --only-matching "https?://[^']+")
    
    if ! echo "$exisiting_origins" | grep --fixed-strings --quiet "$origin" ; then
      next_id=$((next_id+1))
      sqlite3 -batch "$db" "INSERT INTO moz_perms VALUES($next_id, $line);"
    fi
  done < "$2"
  
  notify-send --urgency=normal "Restored Firefox cookie permissions backup" "Successfully restored Firefox' allowed origins for cookie and site data from file \`$2\`."

else
  echo "ERROR: Database is locked. Make sure to quit Firefox before running this script."
  notify-send --urgency=critical "Unable to restore backup" "Database is locked. Make sure to quit Firefox before running this script."
  exit 1
fi

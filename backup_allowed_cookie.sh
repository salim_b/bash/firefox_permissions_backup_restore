#!/bin/bash
#
# Back up Firefox' allowed origins for cookie and site data
#
# Parameters:
#
# - `$1`: Path to the Firefox profile directory.
# - `$2`: Path to write the *allowed Firefox cookie and site data origins* backup file to.

# ensure 1st param is given
if [[ -z "$1" ]] ; then
  echo "No primary argument provided. You need to specify the path to a Firefox profile directory."
  exit 1
fi
# ensure 2nd param is given
if [[ -z "$2" ]] ; then
  echo "No secondary argument provided. You need to specify the filepath to write the allowed Firefox cookie origins backup to."
  exit 1
fi

## ensure all required CLI tools are available
if ! command -v grep >/dev/null ; then
  echo "\`grep\` is required but not found on PATH."
  exit 1
fi
if ! command -v notify-send >/dev/null ; then
  echo "\`notify-send\` is required but not found on PATH."
  exit 1
fi
if ! command -v sqlite3 >/dev/null ; then
  echo "\`sqlite3\` is required but not found on PATH."
  exit 1
fi

db="$1/permissions.sqlite"

if ! sqlite3 -bail -batch "$db" ".dbinfo" 2>&1 | grep --quiet 'database is locked' ; then
  sqlite3 -separator , -batch "$db" "SELECT QUOTE(origin), QUOTE(type), permission, expireType, expireTime, modificationTime FROM moz_perms WHERE type = 'cookie' AND permission = '1';" > "$2"
  notify-send --urgency=normal "Backed up Firefox cookie permissions" "Successfully backed up Firefox' allowed origins for cookie and site data to file \`$2\`."
else
  echo "ERROR: Database is locked. Make sure to quit Firefox before running this script."
  notify-send --urgency=critical "Unable to perform backup" "Database is locked. Make sure to quit Firefox before running this script."
  exit 1
fi
